#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t result_len = (lhs_len > rhs_len) ? lhs_len : rhs_len;
  char *result = malloc(result_len + 2);  // +1 for possible carry, +1 for null terminator
  if (result == NULL) {
      debug_abort("Failed to allocate memory in arithmatoy_add");
  }
  result[result_len + 1] = '\0';
  unsigned int carry = 0;
  for (size_t i = 1; i <= result_len; ++i) {
      unsigned int lhs_digit = (i <= lhs_len) ? get_digit_value(lhs[lhs_len - i]) : 0;
      unsigned int rhs_digit = (i <= rhs_len) ? get_digit_value(rhs[rhs_len - i]) : 0;
      unsigned int sum = lhs_digit + rhs_digit + carry;
      carry = sum / base;
      sum %= base;
      result[result_len - i + 1] = to_digit(sum);
  }
  if (carry > 0) {
      result[0] = to_digit(carry);
      return result;
  }
  return drop_leading_zeros(result + 1);
  
  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {

   if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  if (strcmp(lhs, rhs) == 0) {
    char *result = malloc(2);
    result[0] = '0';
    result[1] = '\0';
    return result;
  }
  else if (strcmp(rhs, "0") == 0) {
    return strdup(lhs);
  }

  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  if (len_lhs < len_rhs || (len_lhs == len_rhs && strcmp(lhs, rhs) < 0)) {
    return NULL;
  }

  size_t max_len = (len_lhs > len_rhs) ? len_lhs : len_rhs;
  char *result = calloc(max_len + 2, sizeof(char));
  int rest = 0;
  int result_index = max_len - 1;

  while (len_lhs > 0 || len_rhs > 0) {
    size_t lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
    size_t rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);

    int difference = lhs_digit - rhs_digit - rest + base;
    rest = (difference >= base ? 0 : 1);
    size_t digit = difference % base;

    result[result_index--] = to_digit(digit);
  }

  while (result[result_index + 1] == '0') {
    ++result_index;
  }

  if (result_index == max_len - 1) {
    free(result);
    return NULL;
  } else {
    char *final_result = strdup(result + result_index + 1);
    free(result);
    return final_result;
  }
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  const size_t lhs_length = strlen(lhs);
  const size_t rhs_length = strlen(rhs);
  const size_t result_length = lhs_length + rhs_length;
  char *result = (char *)malloc((result_length + 1) * sizeof(char));

  if (result == NULL) {
    debug_abort("Memory allocation failed");
  }

  for (size_t i = 0; i <= result_length; ++i) {
    result[i] = '0';
  }

  for (size_t i = 0; i < lhs_length; ++i) {
    unsigned int lhs_digit = get_digit_value(lhs[lhs_length - i - 1]);
    unsigned int carry = 0;

    for (size_t j = 0; j < rhs_length; ++j) {
      unsigned int rhs_digit = get_digit_value(rhs[rhs_length - j - 1]);
      unsigned int product = lhs_digit * rhs_digit + carry;
      unsigned int current_digit = get_digit_value(result[result_length - i - j - 1]);
      unsigned int sum = product + current_digit;
      carry = sum / base;
      result[result_length - i - j - 1] = to_digit(sum % base);
    }

    if (carry > 0) {
      result[result_length - i - rhs_length - 1] = to_digit(carry);
    }
  }

  result[result_length] = '\0';

  return drop_leading_zeros(result);

}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
